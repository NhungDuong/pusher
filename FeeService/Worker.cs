﻿using Confluent.Kafka;
using FeeService.Models;
using System.Text.Json;

namespace FeeService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConsumer<Ignore, string> _consumer;
        private string _topic = "internal-topic";
        private IProducer<string, string> _producer;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
            var config = new ConsumerConfig
            {
                BootstrapServers = "10.26.2.176:9091, 10.26.2.176:9092, 10.26.2.176:9093",
                GroupId = "fee-internal",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            _consumer = new ConsumerBuilder<Ignore, string>(config).Build();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Factory.StartNew(() =>
            {
                _consumer.Subscribe(_topic);

                while (!stoppingToken.IsCancellationRequested)
                {
                    try
                    {
                        var consumeResult = _consumer.Consume(stoppingToken);

                        if (consumeResult.Message != null)
                        {
                            string kafkaMessage = consumeResult.Message.Value;

                            // Call API và xử lý phản hồi
                            var response = ResponseResult();
                            var config = new ProducerConfig
                            {
                                BootstrapServers = "10.26.2.176:9091, 10.26.2.176:9092, 10.26.2.176:9093",
                                LingerMs = 0,
                                EnableIdempotence = true,
                                MaxInFlight = 5,
                                MessageTimeoutMs = 120000,
                                TransactionTimeoutMs = 300000,
                                Acks = Acks.All
                            };

                            _producer = new ProducerBuilder<string, string>(config).Build();
                            string topic = "fee-service";
                            var json = JsonSerializer.Serialize(response);
                            var message = new Message<string, string> { Value = json };
                            _producer.Produce(topic, message);
                        //Console.WriteLine($"{json}");
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        private Response ResponseResult()
        {
            Response response = new Response(true, 1, "Susscess");
            return response;
        }
    }

}