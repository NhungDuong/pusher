﻿namespace MarMor.Models
{
    public class User
    {
        public string ClientCode { get; set; }
        public decimal CashBalance { get; set; }
        public decimal HoldCash { get; set; }
    }
}