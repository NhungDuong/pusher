﻿using System;

namespace MarMor.Models
{
    public class MarMorPayDebit
    {
        public string TermCode { get; set; } 
        public string SercuritiesCode { get; set; }
        public decimal? Debt { get; set; }
        public decimal Interest { get; set; } 
        public decimal Sum { get; set; }
        public string ClientCode { get; set; }

        public MarMorPayDebit() { }

        public MarMorPayDebit(string termCode, string sercuritiesCode, decimal sum, string clientCode)
        {
            TermCode = termCode;
            SercuritiesCode = sercuritiesCode;
            Sum = sum;
            ClientCode = clientCode;
        }
    }
}