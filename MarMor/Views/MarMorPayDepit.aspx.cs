﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using MarMor.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MarMor.Views
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private IProducer<string, string> producer;

        protected void Page_Load(object sender, EventArgs e)
        {
                if (!IsPostBack)
                {
                    //fake data GridView
                    List<MarMorPayDebit> fixedData = new List<MarMorPayDebit>
                {
                     new MarMorPayDebit { TermCode = "MAR.MOR.12345", SercuritiesCode = "AAAA", Debt = 1234000, Interest = 2},
                     new MarMorPayDebit { TermCode = "MAR.MOR.23456", SercuritiesCode = "BBBB", Debt = 2650030, Interest = 1.4m },
                     new MarMorPayDebit {TermCode = "MAR.MOR.34567", SercuritiesCode = "CCCC", Debt = 46879873, Interest = 1.3m}
                };

                    GridViewMarMor.DataSource = fixedData;
                    GridViewMarMor.DataBind();
                }
        }
        protected void AmountPaid_TextChanged(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((TextBox)sender).NamingContainer;
            decimal debt = Convert.ToDecimal(row.Cells[3].Text); //cột Dư nợ phải trả
            decimal interest = Convert.ToDecimal(row.Cells[5].Text); //cột Lãi vay

            TextBox amountPaidTextBox = (TextBox)row.FindControl("AmountPaid");
            decimal amountPaid = string.IsNullOrEmpty(amountPaidTextBox.Text) ? 0 : Convert.ToDecimal(amountPaidTextBox.Text);
            decimal sum = amountPaid * interest; // tính Tổng tiền
            decimal clientCash = Convert.ToDecimal(ClientCashBlance.Text);
            if (sum > debt)
            {
                amountPaidTextBox.Text = debt.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Số tiền trả vượt quá dư nợ!');", true);
            }
            //if (sum > clientCash)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Số tiền phải trả vượt quá số dư tiền!');", true);
            //}

            row.Cells[6].Text = sum.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "calculateTotal", "calculateTotal();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "calculateTotal", "calculateTotal();", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GridViewMarMor.Visible = true;
        }

        protected void ClientCode_TextChanged(object sender, EventArgs e)
        {
            string enteredClientCode = ClientCode.Text;

            string foundUserName = GetUserByClientCode(enteredClientCode);
            decimal clientCash = GetClientCash(enteredClientCode);

            UserName.Text = foundUserName;
            ClientCashBlance.Text = clientCash.ToString();
        }

        private decimal GetClientCash(string enteredClientCode)
        {
            return 100_000_000;
        }

        private string GetUserByClientCode(string clientCode)
        {
            return clientCode;
        }
        protected void GridViewMarMor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowIndex % 2 == 0)
            {
                CheckBox checkBoxSelect = (CheckBox)e.Row.FindControl("CheckBoxSelect");
                TextBox amountPaid = (TextBox)e.Row.FindControl("AmountPaid");
                int idx = e.Row.RowIndex + 1;
                checkBoxSelect.Attributes.Add("onclick", $"ValidateCheck('" + checkBoxSelect.ClientID + "','" + amountPaid.ClientID + "','" + idx.ToString() + "');");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string clientCode = ClientCode.Text;
            if (!string.IsNullOrEmpty(clientCode))
            {
                SendKafkaEvent(clientCode);
                ConsumeEvent();
            }
        }

        private void ConsumeEvent()
        {
            try
            {
                string topic = "fee-account";
                var config = new ConsumerConfig
                {
                    BootstrapServers = "10.26.2.176:9091, 10.26.2.176:9092, 10.26.2.176:9093",
                    GroupId = "oms-internal",
                    AutoOffsetReset = AutoOffsetReset.Earliest
                };
                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    consumer.Subscribe(topic);

                    // Vòng lặp để liên tục consume messages
                    while (true)
                    {
                        try
                        {
                            var result = consumer.Consume();

                            if (result.Message != null)
                            {
                                var a = lblMessage.Text;
                                var resultObject = JObject.Parse(result.Message.Value);
                                var messageValue = resultObject["Message"].ToString();
                                lblMessage.Text = messageValue;
                            }
                            else
                            {
                               lblMessage.Text = "error";
                            }
                        }
                        catch (ConsumeException ex)
                        {
                            // Xử lý exceptions của Kafka Consumer
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendKafkaEvent(string clientCode)
        {
            try
            {
                var config = new ProducerConfig
                {
                    BootstrapServers = "10.26.2.176:9091, 10.26.2.176:9092, 10.26.2.176:9093",
                    LingerMs = 0,
                    EnableIdempotence = true,
                    MaxInFlight = 5,
                    MessageTimeoutMs = 120000,
                    TransactionTimeoutMs = 300000,
                    Acks = Acks.All
                };

                producer = new ProducerBuilder<string, string>(config).Build();
                var msg = new List<Message<string, string>>();
                string topic = "internal-topic";
                foreach (GridViewRow row in GridViewMarMor.Rows)
                {
                    CheckBox checkBox = (CheckBox)row.FindControl("CheckBoxSelect");

                    if (checkBox.Checked)
                    {
                        string termCode = row.Cells[1].Text;
                        string securitiesCode = row.Cells[2].Text;
                        string total = row.Cells[6].Text;

                        MarMorPayDebit data = new MarMorPayDebit(termCode, securitiesCode, Convert.ToDecimal(total), clientCode);
                        var json = JsonSerializer.Serialize(data);
                        var message = new Message<string, string> { Value = json };
                        msg.Add(message);
                    }
                }
                if(msg.Count > 0)
                {
                    foreach(var data in msg)
                    {
                        producer.Produce(topic, data);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}