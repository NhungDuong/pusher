﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarMorPayDepit.aspx.cs" Inherits="MarMor.Views.WebForm1" %>

<link rel="stylesheet" type="text/css" href="../Styles/layout.css" />

<script type="text/javascript">
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function calculateTotal() {

        //alert('CalSum');
        var tbl = document.getElementById('<%=GridViewMarMor.ClientID%>');
        var fTotalPaid = 0;

        for (var j = 1; j <= tbl.rows.length - 1; j++) {
            var _totalPaid = tbl.rows[j].cells[6].innerText.toString().replace(/,/g, '');
            fTotalPaid = fTotalPaid + parseFloat(_totalPaid);
        }
        document.getElementById('<%=TotalPaid.ClientID%>').value = addCommas(fTotalPaid.toString());
    }

    //Khi check vao hop checkbox thi mo textbox de nhap du lieu
    function ValidateCheck(checkObject, textObject, rowIdx) {
        var dg = document.getElementById("<%=GridViewMarMor.ClientID%>");
        var j = rowIdx - 1;
        if (document.getElementById(checkObject).checked == true) {
            document.getElementById(textObject).disabled = false;

            document.getElementById("divContent_GridViewMarMor_AmountPaid_" + j).style.backgroundColor = "#ffffff";
        }
        else {
            document.getElementById(textObject).value = 0;
            document.getElementById(textObject).disabled = true;
            dg.rows[rowIdx].cells[col_TileVayDK].innerHTML = 0;
            dg.rows[rowIdx].cells[col_LaiVay].innerHTML = 0;
            dg.rows[rowIdx].cells[col_TongTien].innerHTML = 0;

            document.getElementById("divContent_GridViewMarMor_AmountPaid_" + j).style.backgroundColor = "#cccccc";
            calculateTotal();
        }

    }

    function showProcessingMessage() {
        document.getElementById('<%= lblMessage.ClientID %>').innerText = 'Đang xử lý...';
    }
</script>

<body>
    <form id="form1" runat="server">
        <div id="divContent" class="box" runat="server">
            <div class="content" style="text-align: left">
                <asp:Label ID="lblTieuDeTrang" runat="server" CssClass="Box_HeaderBL3">TRẢ TIỀN MARMOR</asp:Label>
                <br />
                <br />

                <table id="tblControls" class="form_group" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="w100">
                            <asp:Label ID="Label10" runat="server" CssClass="fL mT6 cl s12">Số tài khoản</asp:Label>
                        </td>
                        <td class="w135">
                            <asp:TextBox ID="ClientCode" runat="server" AutoPostBack="true" MaxLength="10" Width="100%" OnTextChanged="ClientCode_TextChanged">058C</asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w100">
                            <asp:Label ID="Label14" runat="server" CssClass="fL mT6 cl s12">Tên khách hàng</asp:Label></td>
                        <td class="w200" colspan="5">
                            <asp:TextBox ID="UserName" ReadOnly="true" CssClass="txtRO" runat="server" Width="100%"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w100">
                            <asp:Label ID="Label20" runat="server" CssClass="fL mT6 cl s12" Width="100%">Xử lý?</asp:Label></td>
                        <td class="w135">
                            <asp:DropDownList ID="Level" runat="server" Width="100%">
                                <asp:ListItem Value="-1">Tất cả</asp:ListItem>
                                <asp:ListItem Value="0">An toàn</asp:ListItem>
                                <asp:ListItem Value="1">Cảnh báo</asp:ListItem>
                                <asp:ListItem Value="2" Selected="True">Xử lý</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="w100">
                            <asp:Label ID="Label11" runat="server" CssClass="fL mT6 cl s12">Số hợp đồng</asp:Label></td>
                        <td class="w135">
                            <asp:TextBox ID="TermCode" runat="server" Width="100%"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w135">
                            <asp:Label ID="Label15" runat="server" CssClass="fL mT6 cl s12">Mã CK</asp:Label></td>
                        <td class="200">
                            <asp:TextBox ID="SercuritiesCode" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w100">
                            <asp:Label ID="Label1" runat="server" CssClass="fL mT6 cl s12">Từ ngày</asp:Label></td>
                        <td class="w135">
                            <asp:TextBox ID="FromDate" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w100">
                            <asp:Label ID="Label2" runat="server" CssClass="fL mT6 cl s12">Đến ngày</asp:Label></td>
                        <td class="w135">
                            <asp:TextBox ID="ToDate" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="w100">
                            <asp:Label ID="Label9" runat="server" CssClass="fL mT6 cl s12">Loại vay</asp:Label></td>
                        <td class="w135">
                            <asp:DropDownList ID="LoanType" runat="server" Width="100%">
                                <asp:ListItem Value="-1" Selected="True">Tất cả</asp:ListItem>
                                <asp:ListItem Value="0">HĐ KQ thông thường</asp:ListItem>
                                <asp:ListItem Value="1">HĐ KQ T+</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="5" valign="top">
                            <table>
                                <tr>
                                    <td class="w311">
                                        <div>
                                            <asp:Button CssClass="btn btn-primary" ID="btnsearch" runat="server" ValidationGroup="grpvalidate_btnsearch"
                                                Text="&nbsp;Tìm kiếm&nbsp;" OnClick="btnSearch_Click" />
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="w135">
                            <asp:Label ID="Label4" runat="server" CssClass="fL mT6 cl s12">Hỗ trợ tính số tiền phải trả:</asp:Label></td>

                        <td class="w200">
                            <asp:DropDownList ID="Support" runat="server" onchange='OnChange(document.getElementById("<%=ddlSupport.ClientID%>"));'>
                                <asp:ListItem Value="-1" Selected="True">Chọn</asp:ListItem>
                                <asp:ListItem Value="0">Trả hết mức xử lý</asp:ListItem>
                                <asp:ListItem Value="1">Trả hết mức Cảnh báo</asp:ListItem>
                                <asp:ListItem Value="2">Trả tất toán</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                        <td class="w20"></td>
                        <td class="w135"></td>

                        &nbsp;</td>
                    </tr>
                </table>
            </div>
            <div class="contentgrid" style="text-align: left">
                <asp:GridView ID="GridViewMarMor" runat="server" AutoGenerateColumns="False" Visible="false" Width="100%" AllowSorting="True" AllowPaging="false"
                    CellSpacing="1" GridLines="Both" CellPadding="2"
                    BorderWidth="0px" BorderColor="White" HorizontalAlign="Center" AlternatingItemStyle-CssClass="GridAlterItemR" ItemStyle-CssClass="GridItem" OnRowDataBound="GridViewMarMor_RowDataBound">
                    <HeaderStyle HorizontalAlign="Center" CssClass="GridHeaderBC"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TermCode" HeaderText="Số HĐ" ReadOnly="True" />
                        <asp:BoundField DataField="SercuritiesCode" HeaderText="Mã CK" ReadOnly="true" />
                        <asp:BoundField DataField="Debt" HeaderText="Dư nợ còn phải trả" ReadOnly="true" />
                        <asp:TemplateField HeaderText="Số tiền trả gốc">
                            <ItemTemplate>
                                <asp:TextBox ID="AmountPaid" runat="server" AutoPostBack="true" OnTextChanged="AmountPaid_TextChanged"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Interest" HeaderText="Lãi vay" ReadOnly="true" />
                        <asp:BoundField DataField="Sum" HeaderText="Tổng tiền" ReadOnly="true" />
                    </Columns>
                    <AlternatingRowStyle BackColor="#E0E0E0" />
                    <RowStyle BackColor="#F0F0F0" />
                </asp:GridView>
            </div>
            <table id="tblGrid1" cellspacing="0" cellpadding="1" class="box_detail">
                <tr>
                    <td class="w135">
                        <asp:Label ID="Label3" runat="server" CssClass="fL mT6 cl s12">Số dư tiền:</asp:Label>
                    </td>
                    <td class="w200">
                        <asp:TextBox ID="ClientCashBlance" ReadOnly="true" CssClass="txtRO" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td class="w20"></td>
                    <td class="w135">
                        <asp:Label ID="Label5" runat="server" CssClass="fL mT6 cl s12">Tổng tiền trả:</asp:Label></td>
                    <td class="w200">
                        <asp:TextBox ID="TotalPaid" ReadOnly="true" CssClass="txtRO" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="w135">
                        <asp:Label ID="Label6" runat="server" CssClass="fL mT6 cl s12">Phương thức</asp:Label></td>
                    <td class="w200">
                        <asp:DropDownList ID="TradingAcc" runat="server" onchange="tradingAcc_Change()">
                            <asp:ListItem Text="Chọn" Value="Choose"></asp:ListItem>
                            <asp:ListItem Text="Phone" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Floor" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="w135">
                        <asp:Button CssClass="btn btn-primary" ID="btnSave" runat="server"
                            Text="Gửi lệnh" OnClientClick="showProcessingMessage();" OnClick="btnSave_Click" />
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

