﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeeServiceAccount.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }

        public Response()
        {

        }
        public Response(bool success, int code, string message)
        {
            Success = success;
            Code = code;
            Message = message;
        }
    }

  
}
